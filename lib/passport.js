const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const {Admin} = require("../models/user");

// Call back untuk LocalStrategy
async function authenticate(username, password, done) {
	try {
		const admin = await Admin.authenticate({username, password});
		return done(null, admin);
	} catch (err) {
		return done(null, false, {message: err.message});
	}
}

// Untuk autentikasi ketika login di view template
passport.use(new LocalStrategy({usernameField: "username", passwordField: "password"}, authenticate));

// Serialize dan Deserialize Cara untuk membuat sesi dan menghapus sesi
passport.serializeUser((admin, done) => done(null, admin.id));
passport.deserializeUser(async (id, done) => done(null, await Admin.findByPk(id)));
module.exports = passport;
