const {User} = require("../../models");

function format(user) {
	const {id, username} = user;
	return {id, username, accessToken: user.generateToken()};
}

class AuthController {
	register = async (req, res) => {
		User.register(req.body)
			.then(() => {
				res.status(200);
				res.send("data berhasil disimpan");
			})
			.catch((err) => res.send(err.message));
	};

	login = (req, res) => {
		User.authenticate(req.body)
			.then((user) => {
				res.status(200);
				res.json(format(user));
			})
			.catch((err) => res.send(err));
	};

	logout = (req, res) => {
		req.logout();
		res.redirect("/");
	};

	delete = (req, res) => {
		User.destroy({
			where: {id: req.params.id},
		}).then(() => {
			res.status(200);
			res.send("berhasil dihapus");
		});
	};

	whoamiApi = (req, res) => {
		const currentUser = req.body;
		res.json(currentUser);
	};
}

module.exports = AuthController;
