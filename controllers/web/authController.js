const {Admin} = require("../../models");
const passport = require("../../lib/passport");

class AuthController {
	registering = (req, res, next) => {
		Admin.register(req.body)
			.then(() => {
				res.redirect("/login");
			})
			.catch((err) => next(err));
	};

	logging = passport.authenticate("local", {
		successRedirect: "/",
		failureRedirect: "/",
		failureFlash: true,
	});
}

module.exports = AuthController;
