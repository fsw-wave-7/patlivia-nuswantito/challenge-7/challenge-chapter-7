"use strict";

module.exports = {
	up: async (queryInterface, Sequelize) => {
		Example: await queryInterface.bulkInsert(
			"Biodata",
			[
				{
					user_id: 1,
					name: "Tito",
					age: 27,
					createdAt: new Date(),
					updatedAt: new Date(),
				},
			],
			{}
		);
	},

	down: async (queryInterface, Sequelize) => {
		/**
		 * Add commands to revert seed here.
		 *
		 * Example:
		 * await queryInterface.bulkDelete('People', null, {});
		 */
	},
};
